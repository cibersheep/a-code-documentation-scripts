# A-code Documentation Scripts

## Getting started

Run `./GetDocFile.sh` to download html documentation files, generate a Cover with the current date and create epub and pdf files

The script requires to have [Calibre](https://calibre-ebook.com/download_linux) and [convert](https://linux.die.net/man/1/convert) installed in the system

## TO DO:
- Create a version with `pandoc`?
- Check TOC section on the [manual](https://manpages.ubuntu.com/manpages/bionic/man1/ebook-convert.1.html)

