#!/bin/bash
#
# 21 Feb 2022   MLA     # Enhanced version
# 20 Feb 2022   JC      # Initial coding
#
PATH='/bin:/usr/bin:/sbin:/usr/sbin'
site=mipmip.org
log=doc.log

function die
{
  [ $# != 0 ] && echo $* || echo Something has gone wrong!
  [ -f $log ] && echo Please see $log for details.
  exit 1
}

# Check calibre and convert are installed
#
for prog in convert ebook-convert; do
  which $prog >/dev/null 2>&1
  [ $? != 0 ] && die $prog not installed!
done

# Get html documentation files.
#
echo Retrieving A-code documentation...
rm -rf doc
wget -r -np -nc -P doc -nd $site/acode/doc/index.html >$log 2>&1
[ ! -d ./doc ] && die Retrieval failed!

# Find current A-code version and documentation date.
#
echo "Finding out current A-code version."
[ ! -f doc/index.html ] && echo Cannot find doc/index.html!
set -- $(grep '<!-- INFO' doc/index.html 2>>$log)
[ $# != 5 ] && die Unable to retrieve version and date!
version=$3
date=$4
name=acode-$date

# Strip off unwanted vits.
#
echo Removing/replacing unwanted parts of html files...
for f in doc/*.html; do
  sed -i -e '1,/<body /c<html><head><style>\n.centered {text-align:center}\n.hung {text-indent:-0.5cm; margin-left:1cm}\n.indented {margin-left:0.5cm}\n.indented2 {margin-left:1.0cm}\n.dotted {border: 1px dotted #000000; border-style: none none dotted; }\ntable.centab {margin:auto}\n</style>\n</head>\n<body>' \
         -e '/<!-- PAYLOAD END -->/,$d' \
         -e '/<!-- CONTENTS_START -->/,/<!-- CONTENTS_END -->/d' $f
done

# Generate cover.
#
echo Generating cover...
cp cover-template.svg cover.svg
sed -i "s/CURRENT_VERSION/$version/" cover.svg
convert cover.svg cover.png 2>>$log || die Failed to create cover.svg!
rm cover.svg

# Create the epub version.
#
echo Creating $name.epub...
rm -f acode*.epub
ebook-convert doc/index.html $name.epub\
    --level1-toc '//h:h2 | //h:h2[re:test(.,"A-code documentation")]'\
    --level2-toc //h:h3 --level3-toc //h:h4 --no-chapters-in-toc\
    --language en --authors "Mike Arnautov" --author-sort "Arnautov, Mike"\
    --tags "Non-Fiction,Education and Teaching,Documentation"\
    --filter-css bgcolor,background-color\
    --cover cover.png >>$log 2>&1
rm cover.png
[ -f $name.epub ] || die Failed to create $name.epub!

# Create PDF version.
#
echo Creating $name.pdf...
#rm -rf doc acode*.pdf
ebook-convert $name.epub $name.pdf >>$log
[ -f $name.pdf ] || die Failed to create $name.pdf!

# Success!
#
rm -f $log
echo Done!
#
########################## End of script ######################
